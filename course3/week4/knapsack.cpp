#include <bits/stdc++.h>

using namespace std;


int knapsack(vector<int> &P, vector<int> &W, const int &S) {
	int N = P.size();
	vector< vector<int> > dp(N+1, vector<int>(S + 1, 0));
	
	for (int i = 1; i <= N; i++) {
		for (int j = 1; j <= S; j++) {
			if (W[i - 1] > j)
				dp[i][j] = dp[i - 1][j];
			else
				dp[i][j] = max(dp[i - 1][j], P[i - 1] + dp[i - 1][j - W[i - 1]]);
		}
	}	
	
	return dp[N][S];
}

int main()
{
	freopen("knapsack1.txt", "r", stdin);
	//freopen("knapsack_big.txt", "r", stdin);
		
	int S, N;
	cin >> S >> N;
		
	vector<int> W(N); // weights
	vector<int> P(N); // values
	
	for (int i = 0; i < N; i++)
		cin >> P[i] >> W[i];
	
	int opt = knapsack(P, W, S);
	cout << opt << endl;
	
	assert(opt == 2493893);	
	//assert(opt == 4243395);	// big
	return 0;
}
