#include <iostream>
#include <vector>
#include <cassert>

using namespace std;

vector<int> W(2000);  // weight
vector<int> P(2000);  // value
vector< vector<int> > dp(2, vector<int>(2000001, 0));	

int knapsack(int &M, int &S) {
	int p = 0;
	for (int i = 1; i <= M; i++) {
		p = !p;
		for (int j = 1; j <= S; j++) {
			if (W[i - 1] > j)
				dp[p][j] = dp[!p][j];
			else
				dp[p][j] = max(dp[!p][j], P[i - 1] + dp[!p][j - W[i - 1]]);
		}
	}	
	
	return dp[p][S];
}

int main()
{
	int N, K;
	freopen("knapsack_big.txt", "r", stdin);
		
	cin >> K >> N;
	
	for (int i = 0; i < N; i++)
		cin >> P[i] >> W[i];
		
	int opt = knapsack(N, K);
	cout << opt << endl;
	assert(opt == 4243395);
	return 0;
}
