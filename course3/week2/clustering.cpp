#include <bits/stdc++.h>
#include "UF.cpp"

using namespace std;

#define INF INT_MAX
#define numberOfClusters 4
typedef pair<int, int> ii;


typedef tuple<int, int, int> Edge;

int k_clustering(vector<Edge>& edges) {
	UF uf(edges.size());
	
	// sort edges in decreasing order
	sort(edges.begin(), edges.end(), [](const Edge& a, const Edge& b)
	{
		return (get<2>(a) > get<2>(b));
	});

	while (uf.count() > numberOfClusters)
	{
		Edge connect = edges.back();   // min cost edge
		edges.pop_back();
		uf.merge(std::get<0>(connect) - 1, std::get<1>(connect) - 1);   // and connect the points
	}

	// find first cluster not connected to our clusters
	Edge remaining = edges.back();
	while (uf.connected(std::get<0>(remaining) - 1, std::get<1>(remaining) - 1))
	{
		edges.pop_back();
		remaining = edges.back();
	}

	return std::get<2>(remaining);
}
        
vector<int> calculateHammingDistance(int numberOfBits, int d)
{
	std::vector<int> distances;
	// we're interested only in 1 and 2 edge weights
	if (d == 1)
	{
		int dist = 1;
		for (int i = 0; i < numberOfBits; i++)
		{
			distances.push_back(dist);
			dist = dist << 1;
		}
	}
	if (d == 2)
	{
		int dist = 1, mask = 1; // 000...001
		
		for (int i = 0; i < numberOfBits; i++)
		{
			for (int j = i + 1; j < numberOfBits; j++)
			{
				dist = mask | (1 << j);
				distances.push_back(dist);
			}
			mask = mask << 1;
		}
	}

	return distances;
}


void k_clustering_big()
{
	int n, numberOfBits;
	freopen("clustering_big.txt", "r", stdin);
	cin >> n >> numberOfBits;

	string node;
	cin >> node;
	
	unordered_map<int, bool> visited;

	const int MAX_NODES = (int)std::pow(2, numberOfBits);
	UF uf(MAX_NODES); // total number of elements (2^24)

	while (cin >> node)
	{
		node.erase(std::remove_if(node.begin(), node.end(), [](char x) {return std::isspace(x); }), end(node));
		std::bitset<24> bit_node(node);
		int nodeNumber = bit_node.to_ulong();

		visited[nodeNumber] = false;
	}

	// generate distances
	vector<int> distances1 = calculateHammingDistance(numberOfBits, 1);
	vector<int> distances2 = calculateHammingDistance(numberOfBits, 2);
	int edgeCost1 = 0;
	int edgeCost2 = 0;


	// iterate through the map and find the neighbors
	for (auto& el : visited)
	{
		el.second = true;           // we won't bother to add already seen edge to UnionFind

		for (auto d : distances1)
		{
			int res = el.first ^ d;
			auto exists = visited.find(res);
			if (exists != visited.end() && !exists->second) // if it's found but not yet seen!
			{
				// add to UF (there is an edge from current element to the one 1 Hamming distance away)
				edgeCost1++;
				uf.merge(exists->first, el.first);
			}
		}
		for (auto d : distances2)
		{
			int res = el.first ^ d;
			auto exists = visited.find(res);
			if (exists != visited.end() && !exists->second)
			{
				// add node connected to current one 2 Hamming distances away
				edgeCost2++;
				uf.merge(exists->first, el.first);
			}
		}
	}
	
	// from total number of nodes (all clusters)
	// subtract leaders (clusters with nodes that are at most 2 Hamming distances away from each other)
	// and duplicated nodes to get number of clusters that are at least 3 Hamming distances away from others
	size_t totalNumberOfClusters = n - (MAX_NODES - uf.count()) - (n - visited.size());
	std::cout << "Edges 1 Hamming distance away: " << edgeCost1 << std::endl;
	std::cout << "Edges 2 Hamming distances away: " << edgeCost2 << std::endl;
	std::cout << "Number of clusters: " << totalNumberOfClusters << std::endl;
	assert(totalNumberOfClusters == 6118);
	//return totalNumberOfClusters;
}

int main() {
	freopen("clustering_big.txt", "r", stdin);
	/*freopen("clustering1.txt", "r", stdin);
	vector<Edge> edges;
	int n, u, v, w;
	cin >> n;
	
	for (int i = 0; i < n; i++) {
		cin >> u >> v >> w;
		edges.push_back(make_tuple(u, v, w));
	}
	
	int maxSpacing = k_clustering(edges);
	cout << maxSpacing << endl;
	//assert(maxSpacing == 6118);*/
	k_clustering_big();
	return 0;
}
