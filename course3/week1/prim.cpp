#include <bits/stdc++.h>

using namespace std;

#define INF INT_MAX
typedef pair<int, int> ii;

class Graph
{
	int V;
	
	
	public:
		Graph(int V);
		list<ii> *adj;
		
		void addEdge(int u, int v, int w);	
		
		unsigned size() {
			return V;
		}
		
		list<ii> getAdjacencyList(int u) {
			if (u > V)
				throw out_of_range("> V");
				
			return adj[u];
		}
};

Graph::Graph(int V) {
	this->V = V;
	this->adj = new list<ii>[V + 1]; 
}

void Graph::addEdge(int u, int v, int w) {
	adj[u].push_back(make_pair(v, w));
	adj[v].push_back(make_pair(u, w));
}

long prim(Graph& g) {
	long ans = 0;
	vector<int> X = { 1 };  // vertices spanned by tree T so far (initialize X with first vertex in first edge)
	vector<ii> T;			// initially empty (tree so far)
	
	while (X.size() != g.size())
	{
		// let's go over all vertices in X so far and find adjancent min cost nodes
		ii minCostEdge;
		int min = INF;
		for (int u : X)	// always need to find the cheapest edge from whole set of adjacent vertices to set X
		{
			// search min cost edge
			for (auto edge : g.getAdjacencyList(u))
			{
				if (find(X.begin(), X.end(), edge.first) == X.end()) // if target vertex is not in X
				{
					if (edge.second < min)
					{
						minCostEdge = edge;
						min = edge.second;
					}
				}
			}
		}
		
		// add the cheapest node (going out from X) and add destination node to X
		T.push_back(minCostEdge);
		X.push_back(minCostEdge.first);
	}

	for (auto e : T)
		ans += e.second;

	return ans;
}

int main() {
	ifstream in("edges.txt");
    streambuf *cinbuf = cin.rdbuf();
    cin.rdbuf(in.rdbuf());
    	
	int numberOfNodes, numberOfEdges, u, v, w;
	cin >> numberOfNodes >> numberOfEdges;
	
	Graph g(numberOfNodes);
	
	
	for (int i = 0; i < numberOfEdges; i++) {
		cin >> u >> v >> w;
		g.addEdge(u, v, w);
	}

    long mstCost = prim(g);
	cout << "MST cost: " << mstCost << endl;
	assert(mstCost == -3612829);

	cin.rdbuf(cinbuf);
}
