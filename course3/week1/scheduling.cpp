#include<bits/stdc++.h>

#define INF 0x3f3f3f3f

enum ScoreStrategy { Difference, Ratio };

typedef struct Job {
	int weight;
	int length;
	double score;
} Job;

bool compareByScoreAndWeight(const Job& first, const Job& second) {
	if (first.score != second.score)
		return first.score > second.score;
	
	return first.weight > second.weight;
}

double score(int weight, int length, ScoreStrategy strategy) {	
	if (strategy == ScoreStrategy::Ratio)
		return weight / (double) length;
	
	return weight - length;	
}

using namespace std;

long solution(vector<Job>& jobs, ScoreStrategy strategy) {
	for (auto &job : jobs)
		job.score = score(job.weight, job.length, strategy);
		
	sort(jobs.begin(), jobs.end(), compareByScoreAndWeight);
	
	long cumLength = 0;
	long weightedSum = 0;
	
	for (unsigned i = 0; i < jobs.size(); i++)
	{
		cumLength += jobs[i].length;
		weightedSum += jobs[i].weight * cumLength;
	}
	
	return weightedSum;
}

int main() {	
	ifstream in("jobs.txt");
    streambuf *cinbuf = std::cin.rdbuf();
    cin.rdbuf(in.rdbuf());

    int N, weight, length;
    
    cin >> N;
    vector<Job> jobs(N);
    
    
    for (int i = 0; i < N; i++) {
		cin >> weight >> length;
		
		Job job = {
			.weight = weight,
			.length = length,
			.score = 0
		};

		jobs[i] = job;
	}
	
	
	long q1 = solution(jobs, ScoreStrategy::Difference);
	long q2 = solution(jobs, ScoreStrategy::Ratio);
	
	cout << "Schedule by difference: " << q1 << endl;
	cout << "Schedule by ratio: " << q2 << endl;
	
	assert(q1 == 69119377652);
	assert(q2 == 67311454237);
    	
	cin.rdbuf(cinbuf);
}
